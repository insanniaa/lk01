public class Kereta extends Ticket {
    // Tulis kode disini
    private String jenisPemesananTicket;
    private int jumlahTicketTersedia;
    private String nama[] = new String[1000];
    private int q = 0;
    private int r = 0;
    private String kotaAsal[] = new String[1000];
    private String kotaTujuan[] = new String[1000];
    private int jumlahTicketTersedia2;


    public Kereta() {
        this.jumlahTicketTersedia = 1000;
    }

    public Kereta(String jenisPemesananTicket, int jumlahTicketTersedia2) {
        this.jenisPemesananTicket = jenisPemesananTicket;
        this.jumlahTicketTersedia2 = jumlahTicketTersedia2;
    }

    public void tambahTicket(String nama) {
        if (jumlahTicketTersedia != 0 && jumlahTicketTersedia <= 1000) {
            jumlahTicketTersedia--;
            this.nama[q] = nama;
            q++;

            if (jumlahTicketTersedia <= 1000) {
                System.out.println("==============================================");
                System.out.println("Tiket berhasil dipesan");

            } else {
                System.out.println("==============================================");
                System.out.println("Kereta telah habis dipesan");
            }

        }

    }

    public void tambahTicket(String nama, String kotaAsal, String kotaTujuan) {

        if (jumlahTicketTersedia2 != 0 && jumlahTicketTersedia2 <= 30) {
            jumlahTicketTersedia2--;
            this.nama[r] = nama;
            this.kotaAsal[r] = kotaAsal;
            this.kotaTujuan[r] = kotaTujuan;
            r++;
            if (jumlahTicketTersedia2 >= 0 && jumlahTicketTersedia2 <= 30) {
                System.out.println("==============================================");
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: " + jumlahTicketTersedia2);
            } else {
                System.out.println("==============================================");
                System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
            }
        }

    }

    public void printTicket() {
        if (jenisPemesananTicket != null) {

            System.out.println();
            System.out.println("Daftar penumpang kereta api Jayabaya:");
            System.out.println("-------------------------------------");
            for (int i = 0; i < r; i++) {
                System.out.println("nama : " + nama[i]);
                System.out.println("kota Asal : " + kotaAsal[i]);
                System.out.println("kota tujuan : " + kotaTujuan[i]);
                System.out.println("------------------------------------");
            }
        } else {
            System.out.println();
            System.out.println("Daftar penumpang kereta api komuter:");
            System.out.println("------------------------------------");
            for (int i = 0; i < q; i++) {
                System.out.println("nama : " + nama[i]);
            }

        }

    }

}
