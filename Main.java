public class Main {
    // Manipulasi kode pada kelas Ticket dan Kereta agar kode program di main class
    // dapat berjalan
    public static void main(String[] args) {
        // Jangan ubah kedua objek dibawah ini! menambahkan objek diperbolehkan.
        Kereta komuter = new Kereta();
        komuter.tambahTicket("Ahmad Python");
        komuter.tambahTicket("Junaedi Kotlin");
        komuter.tambahTicket("Saiful HTML");
        komuter.printTicket();

        // KAJJ memiliki parameter nama kereta dan jumlah tiket tersedia.
        Kereta KAJJ = new Kereta("Jayabaya", 2);
        KAJJ.tambahTicket("Vania Malinda", "Malang", "Surabaya Gubeng");
        KAJJ.tambahTicket("Sekar SD", "Malang", "Sidoarjo");
        KAJJ.tambahTicket("Bonaventura", "Malang", "Surabaya Pasarturi");
        KAJJ.printTicket();

        // Nama method tambahTiket dan tampilkanTiket tidak perlu diubah, sesuaikan pada
        // kelas Ticket dan Kereta!
    }
}
